class MinBinaryHeap {
    constructor(private arrayOfElements: number[]) {}

    public insert(element: number): void {
        this.arrayOfElements.push(element);
        this.bubbleUp();
    }

    public getMin(): number {
        return this.arrayOfElements[0];
    }

    public extractMin(): number {
        const min: number = this.arrayOfElements[0];
        const end: number = this.arrayOfElements.pop();
        
        if (this.arrayOfElements.length > 0) {
            this.arrayOfElements[0] = end;
            this.sinkDown();
        }

        return min;
    }

    public update(index: number, value: number): void {
        let lastIndex: number = this.arrayOfElements.length - 1;
        if ( index > lastIndex || index < 0 || index % 1 !== 0) {
            console.log('Nothing updated.');
            return;
        } else {
            this.arrayOfElements.splice(index, 1);
            this.insert(value);
        }
    }

    private bubbleUp(): void {
        let elementIndex: number = this.arrayOfElements.length - 1;
        const element: number = this.arrayOfElements[elementIndex];

        while (elementIndex > 0) {
            let parentIndex: number = Math.floor((elementIndex - 1) / 2);
            let parent: number = this.arrayOfElements[parentIndex];
            if (element >= parent) {
                break;
            } else {
                this.arrayOfElements[parentIndex] = element;
                this.arrayOfElements[elementIndex] = parent;
                elementIndex = parentIndex;    
            }
        }
    }

    private sinkDown(): void{
        let index: number = 0;
        const length: number = this.arrayOfElements.length;
        const element: number = this.arrayOfElements[0];

        while (true) {
            let leftChildIndex: number = 2 * index + 1;
            let rightChildIndex: number = 2 * index + 2;
            let leftChild: number;
            let rightChild: number;
            let swap: number = null;

            if (leftChildIndex < length) {
                leftChild = this.arrayOfElements[leftChildIndex];
                
                if (leftChild < element) {
                    swap = leftChildIndex;
                }
            }

            if (rightChildIndex < length) {
                rightChild = this.arrayOfElements[rightChildIndex];
            
                if ( (swap === null && rightChild < element) ||
                     (swap !== null && rightChild < leftChild)
                   ) {
                    swap = rightChildIndex;
                }
            }

            if (swap === null) {
                break;
            } else {
                this.arrayOfElements[index] = this.arrayOfElements[swap];
                this.arrayOfElements[swap] = element;
                index = swap;
            }
        }
    }
}

//test

let heap: MinBinaryHeap = new MinBinaryHeap([1, 2, 4, 5, 6, 7]);
console.log(heap);
heap.insert(0);
console.log("0 inserted");
console.log(heap);
heap.insert(3);
console.log("3 inserted");
console.log(heap);
heap.insert(10);
console.log("10 inserted");
console.log(heap);
console.log(`Actual min value: ${heap.getMin()}`);
heap.update(5, 1);
console.log("Updated index 5 with value 1");
console.log(heap);
console.log(`1.Extracted min value: ${heap.extractMin()}`);
console.log(heap);
console.log(`2.Extracted min value: ${heap.extractMin()}`);
console.log(heap);
console.log(`3.Extracted min value: ${heap.extractMin()}`);
console.log(heap);
console.log(`4.Extracted min value: ${heap.extractMin()}`);
console.log(heap);

//