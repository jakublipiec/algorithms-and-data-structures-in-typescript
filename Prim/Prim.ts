import { MinBinaryHeap } from './MinBinaryHeap/MinBinaryHeap';
import { Edge } from './Edge'

class Prim {
    private priorityQueue: MinBinaryHeap;
    private cost: number;
    constructor() { 
        this.priorityQueue = new MinBinaryHeap([]);
        this.cost = 0;
    }

    public minimumSpanningTreeOf(graph) {
        this.validate(graph);
        
        let unvisitedVertices = this.initializeSetOfVertices(graph.order);
        
        let minimumSpanningTree: number[][] = this.initializeArray(graph.order);

        let startingIndex: number = Math.floor(Math.random() * graph.order)

        while (unvisitedVertices.size !== 0) {
            for (let i = 0; i<graph.order; i++) {
                let wage = graph.matrix[startingIndex][i];
    
                if (wage !== null && minimumSpanningTree[startingIndex][i] === null) {
                    let edge: Edge = {
                        initialVertex: startingIndex, 
                        terminalVertex: i,
                        wage: wage
                    };
                    this.priorityQueue.insert(edge);
                }
            }
            let edge: Edge = this.priorityQueue.extractMin();

            if (unvisitedVertices.has(edge.initialVertex)) {
                unvisitedVertices.delete(edge.initialVertex);
            }
            if (unvisitedVertices.has(edge.terminalVertex)) {
                unvisitedVertices.delete(edge.terminalVertex);
            }

            minimumSpanningTree[edge.initialVertex][edge.terminalVertex] = edge.wage;
            minimumSpanningTree[edge.terminalVertex][edge.initialVertex] = edge.wage;

            this.cost += edge.wage;
    
            startingIndex = edge.terminalVertex;
        }

        return { minimumSpanningTree: minimumSpanningTree, cost: this.cost };
    }

    private validate(graph): void {
        for (let i=0; i<graph.order;i++) {
            for (let j=0; j<graph.order; j++) {
                if (graph.matrix[i][j] !== graph.matrix[j][i]) {
                    throw "This graph is directed."
                } 
                else if (graph.matrix[i][j] !== null) {
                    break;
                } else if (j === graph.order -1) {
                    throw 'This graph is not consistent.'
                }
            }
        }
    }

    private initializeSetOfVertices(numberOfVertices: number): Set<number> {
        let vertices: Set<number> = new Set();
        for (let i=0; i<numberOfVertices; i++) {
            vertices.add(i);
        }
        return vertices;
    }

    private initializeArray(size: number): number[][] {
        let array: number[][] = [];
        for (let i=0; i<size; i++) {
            array[i] = [];
            for (let j=0; j<size; j++) {
                array[i][j] = null;
            }
        }
        return array;
    }
}

let matrix: number[][] = [                                      //  vertex: 0 1 2 3 4
                           [ null, 3   , null, 5   , null ],  // vertex: 0
                           [ 3   , null, 6   , 3   , 7    ],  // vertex: 1
                           [ null, 6   , null, 2   , 4    ],  // vertex: 2
                           [ 5   , 3   , 2   , null, 1    ],  // vertex: 3
                           [ null, 7   , 4   , 1   , null ]   // vertex: 4
                         ];

// matrix[0][1] = 3;
// matrix[0][3] = 5;
// matrix[1][0] = 3;
// matrix[1][2] = 6;
// matrix[1][3] = 3;
// matrix[1][4] = 7;
// matrix[2][1] = 6;
// matrix[2][3] = 2;
// matrix[2][4] = 4;
// matrix[3][0] = 5;
// matrix[3][1] = 3;
// matrix[3][2] = 2;
// matrix[3][4] = 1;
// matrix[4][1] = 7;
// matrix[4][2] = 4;
// matrix[4][3] = 1;
// let order = 5; // vertices: 0, 1, 2, 3, 4

console.log(new Prim().minimumSpanningTreeOf({ matrix: matrix, order: matrix.length }));