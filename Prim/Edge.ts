export interface Edge {
    initialVertex: number;
    terminalVertex: number;
    wage: number;
}