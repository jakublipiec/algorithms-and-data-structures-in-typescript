import { Edge } from '../Edge'

export class MinBinaryHeap {
    constructor(private arrayOfElements: Edge[]) {}

    public insert(element: Edge): void {
        this.arrayOfElements.push(element);
        this.bubbleUp();
    }

    public getMin(): Edge {
        return this.arrayOfElements[0];
    }

    public extractMin(): Edge {
        let min: Edge = this.arrayOfElements[0];
        const end: Edge = this.arrayOfElements.pop();
        
        if (this.arrayOfElements.length > 0) {
            this.arrayOfElements[0] = end;
            this.sinkDown();
        }
        return min;
    }

    public update(index: number, value: Edge): void {
        let lastIndex: number = this.arrayOfElements.length - 1;
        if ( index > lastIndex || index < 0 || index % 1 !== 0) {
            console.log('Nothing updated.');
            return;
        } else {
            this.arrayOfElements.splice(index, 1);
            this.insert(value);
        }
    }

    private bubbleUp(): void {
        let elementIndex: number = this.arrayOfElements.length - 1;
        const element: Edge = this.arrayOfElements[elementIndex];

        while (elementIndex > 0) {
            let parentIndex: number = Math.floor((elementIndex - 1) / 2);
            let parent: Edge = this.arrayOfElements[parentIndex];
            if (element.wage >= parent.wage) {
                break;
            } else {
                this.arrayOfElements[parentIndex] = element;
                this.arrayOfElements[elementIndex] = parent;
                elementIndex = parentIndex;    
            }
        }
    }

    private sinkDown(): void{
        let index: number = 0;
        const length: number = this.arrayOfElements.length;
        const element: Edge = this.arrayOfElements[0];

        while (true) {
            let leftChildIndex: number = 2 * index + 1;
            let rightChildIndex: number = 2 * index + 2;
            let leftChild: Edge;
            let rightChild: Edge;
            let swap: number = null;

            if (leftChildIndex < length) {
                leftChild = this.arrayOfElements[leftChildIndex];
                
                if (leftChild.wage < element.wage) {
                    swap = leftChildIndex;
                }
            }

            if (rightChildIndex < length) {
                rightChild = this.arrayOfElements[rightChildIndex];
            
                if ( (swap === null && rightChild.wage < element.wage) ||
                     (swap !== null && rightChild.wage < leftChild.wage)
                   ) {
                    swap = rightChildIndex;
                }
            }

            if (swap === null) {
                break;
            } else {
                this.arrayOfElements[index] = this.arrayOfElements[swap];
                this.arrayOfElements[swap] = element;
                index = swap;
            }
        }
    }
}